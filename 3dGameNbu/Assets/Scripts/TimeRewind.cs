﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeRewind : MonoBehaviour {

    public bool isRewinding = false;
    [SerializeField]
    float timeRewindable;
    List<Vector3> positions;
    List<Quaternion> rotation;
	// Use this for initialization
	void Start () {
        positions = new List<Vector3>();
        rotation = new List<Quaternion>();

    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Return))
        {
            StartRewind();
        }
        if(Input.GetKeyUp(KeyCode.Return))
        {
            StopRewind();
        }
	}
    void FixedUpdate()
    {
        if(isRewinding)
        {
            Rewind();
        }
        else
        {
            Record();
        }
    }
    public void Rewind()
    {
        if (positions.Count > 0)
        {
            transform.position = positions[0];
            positions.RemoveAt(0);
            transform.rotation = rotation[0];
            rotation.RemoveAt(0);
        }
        else
        {
            StopRewind();
        }
    }
    public void Record()
    {
        if(positions.Count > Mathf.Round(timeRewindable*(1f/ Time.fixedDeltaTime)))
            {
            positions.RemoveAt(positions.Count - 1);
            rotation.RemoveAt(rotation.Count - 1);

        }
        positions.Insert(0, transform.position);
        rotation.Insert(0, transform.rotation);
    }
    public void StartRewind()
    {
        isRewinding = true;
    }
    public void StopRewind()
    {
        isRewinding = false;
    }
}
