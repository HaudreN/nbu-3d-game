﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAI01Tracker : MonoBehaviour {

    public GameObject usedMarker;
    public GameObject Mark01;
    public GameObject Mark02;
    public GameObject Mark03;
    public GameObject Mark04;
    public GameObject Mark05;
    public GameObject Mark06;
    public GameObject Mark07;
    public GameObject Mark08;
    public GameObject Mark09;
    public int MarkTracker;
    public GameObject ourCollide;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (MarkTracker == 0)
        {
            usedMarker.transform.position = Mark01.transform.position;

        }
        if (MarkTracker == 1)
        {
            usedMarker.transform.position = Mark02.transform.position;
        }
        if (MarkTracker == 2)
        {
            usedMarker.transform.position = Mark03.transform.position;
        }
        if (MarkTracker == 3)
        {
            usedMarker.transform.position = Mark04.transform.position;
        }
        if (MarkTracker == 4)
        {
            usedMarker.transform.position = Mark05.transform.position;
        }
        if (MarkTracker == 5)
        {
            usedMarker.transform.position = Mark06.transform.position;
        }
        if (MarkTracker == 6)
        {
            usedMarker.transform.position = Mark07.transform.position;
        }
        if (MarkTracker == 7)
        {
            usedMarker.transform.position = Mark08.transform.position;
        }
        if (MarkTracker == 8)
        {
            usedMarker.transform.position = Mark09.transform.position;
        }

    }

   IEnumerator OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "CarAI01")
        {
            this.GetComponent<BoxCollider>().enabled = false;
            MarkTracker += 1;
            if(MarkTracker == 9)
            {
                MarkTracker = 0;
            }
            yield return new WaitForSeconds(1);
            this.GetComponent<BoxCollider>().enabled = true;
        }
    }
}
